import React, { Fragment } from 'react';

import Hero from '../components/Hero';
import Content from '../components/Content';
import { useAuth0 } from '@auth0/auth0-react';

const Home = () => {
  const { isAuthenticated } = useAuth0();
  if (isAuthenticated)
    return (
      <Fragment>
        <Hero />
        <Content />
      </Fragment>
    );
  return <span>Please Log in</span>;
};

export default Home;
