import React, { useEffect, useState } from 'react';

import { useAuth0 } from '@auth0/auth0-react';

const Timer = () => {
  const { isAuthenticated, loginWithRedirect, logout } = useAuth0();

  const [seconds, setSeconds] = useState(15);

  useEffect(() => {
    console.log('just one');

    if (isAuthenticated) refresh();

    const tick = () => {
      const timeout = new Date(Number(localStorage.getItem('timeout')));
      const now = new Date();

      const secs = Math.round((timeout.getTime() - now.getTime()) / 1000);
      setSeconds(secs);

      console.log('tick', secs, now.getTime(), timeout.getTime());

      if (isAuthenticated && secs < 0) {
        logout({ returnTo: window.location.origin });
      } else {
      }
    };

    const onStorage = function (event) {
      console.log(event);

      if (event.key === 'timeout') {
        if (!isAuthenticated) loginWithRedirect();
      }
    };

    var timerID = setInterval(() => tick(), 1000);
    window.addEventListener('storage', onStorage);

    return () => {
      clearInterval(timerID);
      // window.removeEventListener('storage', onStorage);
    };
  }, [isAuthenticated, loginWithRedirect, logout]);

  const refresh = () => {
    const timeout = new Date();
    timeout.setSeconds(timeout.getSeconds() + 15);
    window.localStorage.setItem('timeout', timeout.getTime());
  };

  return (
    <div className="ml-5">
      {!isAuthenticated && <span>not logged in</span>}
      {isAuthenticated && (
        <span>
          session has {seconds} seconds{' '}
          <button onClick={refresh}>refresh</button>
        </span>
      )}
    </div>
  );
};

export default Timer;
